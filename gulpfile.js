import gulp from "gulp";
import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";

const { src, dest, watch, series, parallel } = gulp;

import bsc from "browser-sync";
const browserSync = bsc.create();
const sass = gulpSass(dartSass);



	
// Збирає усі файли "HTML" і збарає у папку => "Dist"
const htmlTaskHandler = () => {
	return src("./src/*.html").pipe(dest("./dist"));
};


// Уніфікує файл "CSS"
const cssTaskHandler = () => {
	return src("./src/scss/main.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer())
		.pipe(csso())
		.pipe(dest("./dist/css"))
		.pipe(browserSync.stream());
};




// Fonts
const fontTaskHandler = () => {
	return src("./src/fonts/**/*.*").pipe(dest("./dist/fonts"));
};


// Зтискає зображення
// ** — Звернення до будь-якої дочірньої папки
// *. — Звернення до будь-якої назви файлу
// .* — Звернення до будь-якого формату файлу

const imagesTaskHandler = () => {
	return src("./src/images/**/*.*")
		.pipe(imagemin())
		.pipe(dest("./dist/images"));
};

// Превіряє файл "SRS". Змінює старі зміни на нові
const cleanDistTaskHandler = () => {
	return src("./dist", { read: false, allowEmpty: true }).pipe(
		clean({ force: true })
	);
};

// Браузерні функції
const browserSyncTaskHandler = () => {
    // Кажемо "LiveServer" звідки дивитися проєкт
	browserSync.init({
		server: {
			baseDir: "./dist",
		}
	});

    // Перевіряємо нові зміни функцій "CSS"
    // Якщо зміни спрацювали => Перезавантаж сторінку
	watch("./src/scss/**/*.scss").on(
		"all",
		series(cssTaskHandler, browserSync.reload)
    );
    
    // Перевіряємо нові зміни функцій "HTML"
    // Якщо зміни спрацювали => Перезавантаж сторінку
	watch("./src/*.html").on(
		"change",
		series(htmlTaskHandler, browserSync.reload)
    );
    
    // Перевіряємо нові зміни дерикторії "image"
    // Якщо зміни спрацювали => Перезавантаж сторінку
	watch("./src/images/**/*").on(
		"all",
		series(imagesTaskHandler, browserSync.reload)
	);
};

// Мініфікує файл на Production
export const build = series(
	cleanDistTaskHandler,
	parallel(htmlTaskHandler, cssTaskHandler, fontTaskHandler, imagesTaskHandler)
);

// 
export const dev = series(build, browserSyncTaskHandler);


